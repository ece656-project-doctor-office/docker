<?php

  $link = mysqli_connect("database", "root", "tiger", 'docker');

  if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    exit;
  }

  $sql = "CREATE TABLE `users` (
    `id` int NOT NULL,
    `username` varchar(50) NOT NULL,
    `password` varchar(255) NOT NULL,
    `created_at` datetime DEFAULT CURRENT_TIMESTAMP
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";

  if (mysqli_query($link, $sql)) {
    echo "Table users created successfully" . PHP_EOL;
  } else {
    echo "Error creating table: " . mysqli_error($link) . PHP_EOL;
  }

  $sql = "INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'admin', 'DocOfficeAdmin', '2021-09-18 19:20:43');";

  if (mysqli_query($link, $sql)) {
    echo "Admin user created successfully" . PHP_EOL;
  } else {
    echo "Error creating admin user: " . mysqli_error($link) . PHP_EOL;
  }

  $sql = "ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);";

  if (mysqli_query($link, $sql)) {
    echo "Alter user table successfully" . PHP_EOL;
  } else {
    echo "Error altering user table: " . mysqli_error($link) . PHP_EOL;
  }

  $sql = "ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;";

  if (mysqli_query($link, $sql)) {
    echo "Alter user table successfully" . PHP_EOL;
  } else {
    echo "Error altering user table: " . mysqli_error($link) . PHP_EOL;
  }
