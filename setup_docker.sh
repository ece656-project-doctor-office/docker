git clone https://github.com/sprintcube/docker-compose-lamp.git
mv docker-compose-lamp/* .
rm -rf docker-compose-lamp
docker-compose up -d
cd www
rm -rf *
rm -rf .*
git clone https://git.uwaterloo.ca/ece656-project-doctor-office/code .
cd ..
cp default.bashrc www
cp .htaccess www
cp setup_tables.php www
